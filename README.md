![Nossa logo](/logo_funto_amarelo.png)

## Instalação

### Instalação do Node

No terminal digite:

```curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.36.0/install.sh | bash```

Se não tiver o `curl` instalado tente por:

```wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.36.0/install.sh | bash```

Feche e abra o terminal

```nvm install node```

### Instalação das dependências``

Clone o repositório https://gitlab.com/hacky-hour/beerbud.git

Para instalar os pacotes necessários execute no diretório `beerbud`:

```npm install```

## Uso

Para executar o programa, basta executar o comando:

```npm start```

A partir disso ele abrirá uma janela no navegador conectado à porta local 3000. Ela abrirá a janela de login, requisitando um email e a senha.

Para modo de experimentação use o usuário:`admin` e a senha: `admin`  

## Funções

Após entrar, você entrará na página principal da aplicação, nela haverão algumas funções que poderão ser executadas, outras que estão em MOC(serão implementadas em versões seguintes)

### Brinde Virtual

Umas das principais features do nosso programa, ela pretende pegar duas fotos de usuários diferentes numa posição de brinde, e juntá-las simulando um brinde num local mais apropriado (no momento só o bar está registrado como local)

Para executar essa função basta clicar no botão `FAÇA SEU BRINDE VIRTUAL!`, ela irá redirecioná-lo para uma página de "Buds", para assim tirar uma foto com el*. Se desejar incluir novos buds, basta clicar em `ADICIONE MAIS AMIGOS!`, para assim adicionar novos buds para brindar.

Após selecionar um bud, você será movido para uma página, em que abrirá sua câmera para tirar a foto. Nela serão mostrados dois quadrados, o maior para sua própria posição e um menor para colocar a sua bebida, assim basta tirar a fota.

Com isso ele irá lhe retorna a foto mesclada entre os dois buds, tendo a opção de compartilhar a foto em suas redes sociais(em MOC)

### Geladeira Virtual

Outra das principais funções é a da geladeira virtual, ela aparece na página principal e serve como uma forma de ver todas as suas bebidas. Futuramente será possivel compartilhar com seus amigos o que você tem na geladeria e pretendemos comunicar seus buds quando estiverem com geadeiras cheias, e assim fazer seu brinde virtual.

Nela você pode ver quais bebidas tem e pode adicionar novas clicando em `ADD`. Futuramente desejamos conectar essa aplicação com outras da AB InBev, assim colocar novas bebidas diretamente na geladeira quando comprados por outras aplicações.

### Gameficação

A gameficação é representada pela quantidade de brindes, o nível e a barra de experiência. A ideia dela é que o consumidor poderia consumir mais bebidas e fazer mais brindes para ganhar promoções, descontos e/ou outros benefícios.

### Funções em MOC

Algumas funções como perfil, compartilhamento e notificações precisaram ficar em MOC, mas desejamos tê-las na aplicação para mais informações do consumidor.


This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
