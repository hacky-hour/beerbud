import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom';

import Appbar from './components/Appbar';
import CameraPage from './components/camera/Camera';
import Home from './components/home/Home';
import Error from './components/error/Error';
import FriendList from './components/list/List';
import Result from './components/camera/Result';
import Perfil from './components/perfil/Perfil';
import Login from './components/Login';
import { isAuthenticated } from './service/auth';

ReactDOM.render(
	<BrowserRouter>
		<Appbar />
		<Switch>
			<Route path="/" component={Home} exact />
			<Route path="/camera/:id/:role" component={CameraPage} />
			<Route path="/list" component={FriendList} />
			<Route path="/result/:id" component={Result} />
			{/* <Route path="/perfil" component={Perfil} /> */}
			<Route path="/login" component={Login} />
			<Route path="/error" component={Error} />	
			<Route component={Error} />
		</Switch>
		
	</BrowserRouter>
  	,document.getElementById('root')
);
