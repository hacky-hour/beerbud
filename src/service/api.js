import axios from "axios"
import { host } from "../config/path"
import { getToken, login, logout } from "./auth"


const api = axios.create({
    baseURL: host
})

api.interceptors.request.use(async config => {
    const token = getToken();
    if (token) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
})

api.interceptors.response.use(response => {
    return response;
  }, error => {
    if (error.response.status === 401) {
      logout();
    }
    return error;
  });

export default api;