const hexToRgb = input => {
    input = input + "";
    input = input.replace("#", "");
    let hexRegex = /[0-9A-Fa-f]/g;
    if (!hexRegex.test(input) || (input.length !== 3 && input.length !== 6)) {
        throw new Error("input is not a valid hex color.");
    }
    if (input.length === 3) {
        let first = input[0];
        let second = input[1];
        let last = input[2];
        input = first + first + second + second + last + last;
    }
    input = input.toUpperCase(input);
    let first = input[0] + input[1];
    let second = input[2] + input[3];
    let last = input[4] + input[5];
    return (
        parseInt(first, 16) +
        ", " +
        parseInt(second, 16) +
        ", " +
        parseInt(last, 16)
    );
};


const primaryColor = ["#F9A129", "#F9A129", "#F9A129", "#F9A129"];
const secundaryColor = ["#FCCC4C", "#FCCC4C"]
const thirdcolor = ["#FCCC4C", "#FCCC4C"]
const blackColor = "#000";
const whiteColor = "#FFF";


const primaryBoxShadow = {
    boxShadow:
        "0 4px 20px 0 rgba(" +
        hexToRgb(blackColor) +
        ",.14), 0 7px 10px -5px rgba(" +
        hexToRgb(primaryColor[0]) +
        ",.4)"
};

const secundaryBoxShadow = {
    boxShadow:
        "0 4px 20px 0 rgba(" +
        hexToRgb(blackColor) +
        ",.14), 0 7px 10px -5px rgba(" +
        hexToRgb(secundaryColor[0]) +
        ",.4)"
};

const thirdBoxShadow = {
    boxShadow:
        "0 4px 20px 0 rgba(" +
        hexToRgb(blackColor) +
        ",.14), 0 7px 10px -5px rgba(" +
        hexToRgb(thirdcolor[0]) +
        ",.4)"
};

const primaryCardHeader = {
    background:
        "linear-gradient(60deg, " + primaryColor[0] + ", " + primaryColor[1] + ")",
        ...primaryBoxShadow
};

const secundaryCardHeader = {
    background:
        "linear-gradient(60deg, " + secundaryColor[0] + ", " + secundaryColor[1] + ")",
        ...secundaryBoxShadow
}

const thirdCardHeader = {
    background:
        "linear-gradient(60deg, " + thirdcolor[0] + ", " + thirdcolor[1] + ")",
        ...thirdBoxShadow
}


const cardActions = {
    margin: "0 20px 10px",
    paddingTop: "10px",
    borderTop: "1px solid #eee",
    height: "auto",
    fontWeight: "300",
    lineHeight: "1.5em"
};

const cardHeader = {
    margin: "-20px 15px 0",
    borderRadius: "3px",
    padding: "15px"
};

const card = {
    display: "inline-block",
    position: "relative",
    width: "100%",
    margin: "25px 0",
    boxShadow: "0 1px 4px 0 rgba(" + hexToRgb(blackColor) + ", 0.14)",
    borderRadius: "3px",
    color: "rgba(" + hexToRgb(blackColor) + ", 0.87)",
    background: whiteColor
};

export {
    hexToRgb,
    card,
    primaryColor,
    blackColor,
    whiteColor,
    primaryBoxShadow,
    primaryCardHeader,
    cardActions,
    cardHeader,
    secundaryCardHeader,
    secundaryBoxShadow,
    thirdBoxShadow,
    thirdCardHeader
};
