import {
    primaryCardHeader,
    whiteColor,
    secundaryCardHeader,
    thirdCardHeader
} from "./GeneralStyle";

const cardHeaderStyle = {
    cardHeader: {
        padding: "0.75rem 1.25rem",
        marginBottom: "0",
        borderBottom: "none",
        background: "transparent",
        zIndex: "3 !important",
        "&$primaryCardHeader": {
            margin: "0 15px",
            position: "relative",
            color: whiteColor,
            borderRadius: "3px",
            marginTop: "-20px",
            padding: "15px"
        },
        "&$secundaryCardHeader": {
            margin: "0 15px",
            position: "relative",
            color: whiteColor,
            borderRadius: "3px",
            marginTop: "-20px",
            padding: "15px"
        },
        "&$thirdCardHeader": {
            margin: "0 15px",
            position: "relative",
            color: whiteColor,
            borderRadius: "3px",
            marginTop: "-20px",
            padding: "15px"
        },
        "&:first-child": {
            borderRadius: "15px",
            textAlign: "center",
            color: "#000"
        },
    },
    primaryCardHeader: {
        color: whiteColor,
        ...primaryCardHeader
    },
    secundaryCardHeader: {
        color: whiteColor,
        ...secundaryCardHeader
    },
    thirdCardHeader: {
        color: whiteColor,
        ...thirdCardHeader
    }
};

export default cardHeaderStyle;
