import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { Link, Redirect } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Fridge from './Fridge';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Progress from '../Progress';
import { isAuthenticated } from '../../service/auth';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  Card: {
    width: 300,
    margin: 'auto'
  },
  Media: {
    height: 550,
    width: '100%',
    objectFit: 'cover'
  }
}));

export default function Home() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const bull = <span className={classes.bullet}>•</span>;

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  if(!isAuthenticated()){
    return <Redirect to="/login" push/>
  }
  
  return (
    <Grid container spacing={0} direction="column" alignItems="center" style={{ minHeight: '93vh', backgroundColor: '#E8EBEB',}}>
      <Grid item style={{ paddingTop: "1rem" }}>
        <Card className={classes.root} elevation={3}>
          <CardContent style={{ textAlign: "center" }}>
            <Typography className={classes.title} color="textSecondary" gutterBottom>
              Palavra do Dia
            </Typography>
            <Typography variant="h5" component="h2">
              di{bull}ver{bull}são
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
            substantivo
            </Typography>
            <img src="https://i.imgur.com/uHOdG1e.png" alt="Comemore!" width="100"/>
          </CardContent>
          <CardContent>
            <Typography variant="body2" color="textSecondary" component="p" style={{ textAlign: "justify" }} >
              Ato ou efeito de se divertir; Desvio do espírito para coisas diferentes das que o preocupam; Ato de voltar para uma e outra parte.
            </Typography>
            <Grid style={{ textAlign: "center", justifyContent: 'center', paddingTop: "1rem" }}>
              <Button variant="contained" pt={2}  style={{ backgroundColor: '#F9A129' }} component={Link} to={'/list'} >
                  <b>Faça seu Brinde Virtual!</b>
              </Button>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
      <Fridge />
      <Grid item>
        <Progress />
      </Grid>
    </Grid>
  );
}