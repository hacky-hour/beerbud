import React, { useEffect, useState } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import PersonIcon from '@material-ui/icons/Person';
import {AddIcon} from '@material-ui/icons/Add';
import { blue } from '@material-ui/core/colors';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import Progress from '../Progress';
import ice from '../../ice.png';
import Badge from '@material-ui/core/Badge';
import api from '../../service/api';
import { imagePath } from '../../config/path';

const drinks_in_fridge = {
  "Skol": [2,"https://static.carrefour.com.br/medias/sys_master/images/images/h1b/h0a/h00/h00/13152856506398.jpg"],
  "Bhrama": [3,"https://images-americanas.b2w.io/produtos/01/00/img1/105027/4/105027424_1GG.jpg"],
  "Bud": [4,"https://savegnago.vteximg.com.br/arquivos/ids/278408-600-600/CERVEJA-BUDWEISER-350ML-LATA.jpg?v=636259642452170000"],
}
const possible_drinks = {
  "Skol": [1,"https://static.carrefour.com.br/medias/sys_master/images/images/h1b/h0a/h00/h00/13152856506398.jpg"],
  "Bhrama": [1,"https://images-americanas.b2w.io/produtos/01/00/img1/105027/4/105027424_1GG.jpg"],
  "Bud": [1,"https://savegnago.vteximg.com.br/arquivos/ids/278408-600-600/CERVEJA-BUDWEISER-350ML-LATA.jpg?v=636259642452170000"]
}

const array_in_fridge = Object.entries(drinks_in_fridge);
const array_possible = Object.entries(possible_drinks);

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
}));

const StyledBadge = withStyles((theme) => ({
  badge: {
    backgroundColor: '#44b700',
    color: '#44b700',
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: '$ripple 1.2s infinite ease-in-out',
      border: '1px solid currentColor',
      content: '""',
    },
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.8)',
      opacity: 1,
    },
    '100%': {
      transform: 'scale(2.4)',
      opacity: 0,
    },
  },
}))(Badge);

const SmallAvatar = withStyles((theme) => ({
  root: {
    width: 22,
    height: 22,
    border: `2px solid ${theme.palette.background.paper}`,
    backgroundColor: '#C2E1EB'
  },
}))(Avatar);

export default function Fridge() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [products, setProducts] = useState([])
  const [userProducts, setUserProducts] = useState([])
  

  useEffect(() => {
    api.get("/listAllProducts")
    .then(res => {
      console.log(res.data);
      setProducts(res.data)
    }).catch(error => {
      console.log(error);
    })
    api.get("/listUserProducts")
    .then(res => {
      console.log(res.data);
      setUserProducts(res.data.filter(e => e.pivot.num_bebidas != 0 && e.pivot.num_bebidas != null))
    }).catch(error => {
      console.log(error);
    })
  }, [])
  
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = (value) => {
    setOpen(false);
  };

  


  function FormRow() {

    const [drinkNumbers, setDrinkNumbers] = useState({
      "1": 0,
      "2": 0,
      "3": 0,
      "4": 0,
      "5": 0,
      "6": 0,
      "7": 0,
      "8": 0,
      "9": 0,
      "10": 0
    })

    const handleChangePlusDrinkQuantity = (event) => {
      setDrinkNumbers({...drinkNumbers, [event.currentTarget.id]: drinkNumbers[event.currentTarget.id] + 1})
    }
  
    const handleChangeMinusDrinkQuantity = (event) => {
      setDrinkNumbers({...drinkNumbers, [event.currentTarget.id]: drinkNumbers[event.currentTarget.id] - 1})
    }

    const updateFridge = async () => {
      const itens = Object.entries(drinkNumbers).filter(e => e[1] != 0)
      console.log(itens);
      for(const body of itens){
        console.log("entrou for");
        console.log(body);
        await api.post("/addBebida", {product_id: parseInt(body[0]), num_bebidas: parseInt(body[1])})
        .then(res => {

        }).catch(error => {
          console.log(error);
        })
      }
      window.location.reload()
    }

    return (
      <React.Fragment>
        <Grid container item xs={12} spacing={3}>
          {userProducts.map((drink) => (
            <Grid item xs={6}>
              <Paper >
                <Grid item>
                <Badge
                  overlap="circle"
                  anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                  }}
                  badgeContent= {drink.pivot.num_bebidas}
                  >
                  <Avatar alt={ drink.name } src={imagePath + drink.image} />
              </Badge>
              </Grid>
              <Grid item>
                  <Button>
                    { drink.name }
                  </Button>
              </Grid>
              </Paper>  
            </Grid>
          ))}
          <Grid item xs={6} onClick={handleClickOpen}>
            <Paper style={{ textAlign: 'center' }}>
            <Grid item>
                <Badge
                  overlap="circle"
                  anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                  }}
                  badgeContent= {' '}
                  >
                  <Avatar>+</Avatar>
                  </Badge>
            </Grid>
            <Grid item>
              <Button >
              Add
              </Button>
            </Grid>
            </Paper>  
            <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
              <DialogTitle id="simple-dialog-title">Escolha sua bebida</DialogTitle>
              <Grid container>
                <Grid item xs = {12}>
                  <List>
                    {products.map((possible) => (
                      <ListItem>
                        <ListItemAvatar>
                          <Avatar alt={possible.name} src={imagePath + possible.image} />
                        </ListItemAvatar>
                        <ListItemText primary={possible.name}/>
                        <Button onClick={handleChangeMinusDrinkQuantity} id={possible.id}>-</Button>{drinkNumbers[possible.id]}<Button onClick={handleChangePlusDrinkQuantity} id={possible.id}>+</Button>
                      </ListItem>
                    ))}
                  </List>
                </Grid>
                <Grid item xs ={12}>
                  <Button variant="contained" pt={2}  style={{ backgroundColor: '#F9A129' }} fullWidth={true} onClick={updateFridge}>
                      <b>Atualizar Geladeira</b>
                  </Button>
                </Grid>
              </Grid>
            </Dialog>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
	return (
    <Grid item style={{ paddingTop: '1rem' }}>
      <div style={{ textAlign: 'center', zIndex: '100'}}>
        <img src={ ice } alt="Geladeira" width="100px" />
      </div>
      <Card style={{ marginTop:'-50px', textAlign: 'center', minWidth: '345px' }} elevation={3}>
          <Typography color="textSecondary" gutterBottom style={{ marginTop:'40px', marginBottom:'10px'}}>
          Seja bem vindo à sua geladeira virtual!
          </Typography>
          <Grid container spacing={1} style={{ marginBottom: '10px'}} justify="center">
            < FormRow />
          </Grid>
      </Card>
    </Grid>
  )
}
