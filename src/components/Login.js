import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Button from '@material-ui/core/Button';
import { host } from '../config/path';
import { login, logout } from '../service/auth';
import api from "../service/api"

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
}));

export default function Login() {
  const classes = useStyles();
  const [formData, setFormData] = useState({email: "", password: ""})

  useEffect(() => {
    logout()
  }, [])

  const handleChange = (event) => {
    setFormData({...formData, [event.target.id]: event.target.value})
  }

  const handleLogin = () => {
    api.post("/authenticate", formData)
    .then(res => {
      login(res.data.token)
      window.location.href = window.location.href.split("log")[0]
    }).catch((error) => {
      logout()
      window.location.href = window.location.href.split("/log")[0] + "/error"
    })

  }

  return (
    <div>
      <div className={classes.margin}>
        <Grid container spacing={10}  justify="center" alignItens="Center" alignItems="center" >
          <Grid item>
            <AccountCircle fontSize="Large"/>
          </Grid>
	    </Grid>
        <Grid container spacing={1} justify="center" alignItems="center">
          <Grid item>
            <TextField 
            id="email" 
            value={formData.email}
            onChange={handleChange}
            label="Email" />
          </Grid>
        </Grid>
      </div>
      <div className={classes.margin}>
        <Grid container spacing={1} justify="center" alignItems="center">
          <Grid item>
            <TextField 
            id="password" 
            value={formData.password}
            onChange={handleChange}
            label="Senha" />
          </Grid>
        </Grid>
		    <Grid style={{ textAlign: "center", justifyContent: 'center', paddingTop: "1rem" }}>
            <Button variant="contained" pt={2}  style={{ backgroundColor: '#F9A129' }} onClick={handleLogin}>
                <b>Entrar</b>
            </Button>
        </Grid>
		
      </div>

    </div>
  );
}
