import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import ice from '../../ice.png';

const styles = {
    loadingBackground: {
        paddingTop: "5rem",
        textAlign: "center"
    }
}

const useStyles = makeStyles(styles);

export default function Loading() {
    const classes = useStyles();
    return (
        <div className={classes.loadingBackground}>
            <Grid item style={{ paddingBottom: '1rem'}}>
                Esfrie aí!
            </Grid>
            <Grid item>
                <img src={ ice } alt="Geladeira" width="30px" />
            </Grid>
            <Grid item style={{ paddingBottom: '5rem'}}>
                Carregando ;)
            </Grid>
            <Grid item>
                <CircularProgress size="8rem" style={{color:"#F9A129"}}/>
            </Grid>
        </div>
    );
}