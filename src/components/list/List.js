import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import PersonIcon from '@material-ui/icons/Person';
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import AddIcon from '@material-ui/icons/Add';
import PropTypes from 'prop-types';
import { blue } from '@material-ui/core/colors';
import { host } from '../../config/path';
import api from '../../service/api';
import Progress from '../Progress';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
	avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
}));

const emails = ['Andreis', 'Marcos', 'Enzo', 'Pedro'];
const add_name = ['16', 'Bah', 'Mari']

function SimpleDialog(props) {
  const classes = useStyles();
  const { onClose, selectedValue, open } = props;

  const [users, setUsers] = useState([])

  const handleClose = () => {
    onClose(selectedValue);
  };

  const handleListItemClick = async (event) => {
    console.log(event.currentTarget.id)
    await api.post("/addfriend", {friend_id: event.currentTarget.id})
    .then(res => {
      window.location.reload()
    }).catch(error => {
      console.log(error);
    })
  };

  useEffect(() => {
    api.get("/users")
    .then(res => {
      console.log("lala");
      console.log(res.data);
      setUsers(res.data)
    }).catch(error => {
      console.log(error);
    })
  }, [])

  return (
    <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
      <DialogTitle id="simple-dialog-title">Subscribe</DialogTitle>
      <List  style={{width:"300px"}}>
        {users.map((user) => (
          <ListItem button onClick={handleListItemClick} key={user.id} id={user.id}>
            <ListItemAvatar>
              <Avatar className={classes.avatar}>
                <PersonIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={user.username} />
            <AddIcon />
          </ListItem>
        ))}

        {/* <ListItem autoFocus button onClick={() => handleListItemClick('addAccount')}>
          <ListItemAvatar>
            <Avatar>
              <AddIcon />
            </Avatar>
          </ListItemAvatar>
         <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancelar
          </Button>
          <Button onClick={handleClose} color="primary">
            Confirmar
          </Button>
	  
        </DialogActions> 

        </ListItem> */}
      </List>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  selectedValue: PropTypes.string.isRequired,
};

export default function FriendList() {
  const classes = useStyles();
  const [selectedIndex, setSelectedIndex] = React.useState(1);
  const [open, setOpen] = React.useState(false);
  const [friends, setFriends] = useState([])


  useEffect(() => {
    const fetchData = () => {
      api.get("/friends")
      .then(res => {
        setFriends(res.data);
      }).catch(error => {
        console.log(error);
      })
    }

    fetchData()
   
  }, [])

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };


  const handleListItemClick = (event) => {
    console.log(event.currentTarget.id);
    api.post("/createsessions", {friend_id: parseInt(event.currentTarget.id)})
    .then(res => {
      window.location.href = window.location.href.split("/lis")[0] + "/camera/" + res.data.id + "/" + res.data.type[0]
    }).catch(error => {
      console.log(error);
    })
  };

  return (
    <Grid container spacing={0} direction="column" alignItems="center" style={{ minHeight: '93vh', backgroundColor: '#E8EBEB',}}>
      <Grid item style={{ paddingTop: "1rem"}}>
      <Card className={classes.root} >
        <CardContent style={{ textAlign: "center",  }}>
            <Typography variant="h6" className={classes.title}>
              Escolha seu Bud!
            </Typography>
            <List component="nav" aria-label="main mailbox folders">
              {friends.map((friend) => (
                <ListItem button onClick={handleListItemClick} key={friend.id} id={friend.id}>
                  <ListItemAvatar>
                    <Avatar className={classes.avatar}>
                      <PersonIcon />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText primary={friend.username} />
                </ListItem>
              ))}
            </List>


      <div>
	  <br />
      <Button variant="outlined" style={{ color: '#D39827' }} onClick={handleClickOpen}>
        Adicione mais amigos!
      </Button>
	  <SimpleDialog open={open} onClose={handleClose} />
      
    </div>
        </CardContent>
      </Card>
      </Grid>
      <Grid item>
        <Progress />
      </Grid>
      
    </Grid>

  );
}
