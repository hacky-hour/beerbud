import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
});

export default function MediaCard() {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image="https://portalcorreio.com.br/wp-content/uploads/2020/01/Gabigol-%C3%A9-o-grande-astro-do-Flamengo-Cr%C3%A9dito-Divulga%C3%A7%C3%A3o-1280x720.jpg"
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            MAIS 25 BRINDES E...
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p" style={{ textAlign: 'justify'}}>
            Sabia que se você comemorar um pouquinho mais usando o BeerBud, você pode ter acesso à geladeira virtual do Gabigol?
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary">
          SAIBA MAIS
        </Button>
        <Button size="small" color="primary">
          OUTROS PREMIOS
        </Button>
      </CardActions>
    </Card>
  );
}