import React from 'react';
import Camera, { FACING_MODES, IMAGE_TYPES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import { useHistory, useParams } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { host } from '../../config/path';
import api from '../../service/api';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
  wrapper: {
    position: 'relative',
  },
  overlay:{
    position: 'absolute',
    zIndex: '100',
    width: '10vmax',
    height: '30vmin',
    top:'1vh',
    right:'1vw',
    borderStyle: 'dotted',
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
    color: '#FFF',
    borderColor: '#FFF'
  },
  bodyoverlay:{
    position: 'absolute',
    zIndex: '100',
    width: '20vmax',
    height: '50vmin',
    bottom:'10vh',
    left:'20vw',
    color: '#FFF',
    borderStyle: 'dotted',
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
    borderColor: '#FFF'
  }
}));

export default function CameraPage(props) {
  const {id, role} = useParams()
  const history = useHistory();
  const classes = useStyles();



  function handleTakePhoto (dataUri) {
    // Do stuff with the photo...
    console.log(dataUri);
  }
 
  function handleTakePhotoAnimationDone (dataUri) {
    const body = role == 'h' ? {foto1: dataUri} : {foto2: dataUri}
    api.post(`/uploadPhoto/${id}`, body)
    .then(res => {
      console.log(res);
      window.location.href = window.location.href.split("/came")[0] + "/result/" + res.data.id
    }).catch(error => {
      console.log(error);
    })
  }
 
  function handleCameraError (error) {
    console.log('handleCameraError', error);
  }
 
  function handleCameraStart (stream) {
    console.log('handleCameraStart');
  }
 
  function handleCameraStop () {
    console.log('handleCameraStop');
  }
 
  return (
    <Grid>
      <Grid container spacing={0} direction="column" alignItems="center" style={{ minHeight: '93vh', backgroundColor: '#E8EBEB',}}>
        <Grid item style={{ paddingTop: "1rem"}}>
          <Card className={classes.root} >
            <CardContent>
              <div className={classes.wrapper}>
                <div className={classes.overlay}>Seu Drink</div>
                <div className={classes.bodyoverlay}>Seu Sorriso :)</div>
                <Camera
                  onTakePhoto = { (dataUri) => { handleTakePhoto(dataUri); } }
                  onTakePhotoAnimationDone = { (dataUri) => { handleTakePhotoAnimationDone(dataUri); } }
                  onCameraError = { (error) => { handleCameraError(error); } }
                  idealFacingMode = {FACING_MODES.ENVIRONMENT}
                  idealResolution = {{width: 640, height: 480}}
                  imageType = {IMAGE_TYPES.JPG}
                  imageCompression = {0.97}
                  isMaxResolution = {true}
                  isImageMirror = {false}
                  isSilentMode = {false}
                  isDisplayStartCameraError = {true}
                  isFullscreen = {false}
                  sizeFactor = {1}
                  onCameraStart = { (stream) => { handleCameraStart(stream); } }
                  onCameraStop = { () => { handleCameraStop(); } }
                />
              </div>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Grid>
  );
}