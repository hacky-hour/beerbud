import React, { useEffect, useState } from 'react';
import Camera, { FACING_MODES, IMAGE_TYPES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import PersonIcon from '@material-ui/icons/Person';
import { useParams } from 'react-router-dom';
import { host, imagePath } from '../../config/path';
import Loading from '../Loading/Loading';
import { setIntervalAsync, clearIntervalAsync } from 'set-interval-async/dynamic'
import api from '../../service/api';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import FacebookIcon from '@material-ui/icons/Facebook';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import InstagramIcon from '@material-ui/icons/Instagram';
import Rewards from './Rewards'


const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 345,
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
      objectFit: 'cover'
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    avatar: {
      backgroundColor: red[500],
    },
  }));

  const StyledMenu = withStyles({
    paper: {
      border: '1px solid #d3d4d5',
    },
  })((props) => (
    <Menu
      elevation={0}
      getContentAnchorEl={null}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      {...props}
    />
  ));
  
  const StyledMenuItem = withStyles((theme) => ({
    root: {
      '&:focus': {
        backgroundColor: '#F9A129'
      },
    },
  }))(MenuItem);

export default function CameraPage(props) {
    const {id} = useParams()
    var date_today = new Date();
    const classes = useStyles();
    const [expanded, setExpanded] = useState(false); 
    const [loading, setLoading] = useState(true) 
    const [friend, setFriend] = useState({}) 
    const [img, setImg] = useState("") 
    const [anchorEl, setAnchorEl] = React.useState(null);

    useEffect(() => {
      let interval;

      const fetchData = async () => {
        console.log("entrou fetchData");
        await api.get(`/result/${id}`)
        .then(res => {
          console.log(res.data);
          if(res.data.status){
            setImg(res.data.foto1 + res.data.foto2)
            setFriend(res.data.friend)
            setLoading(false)
            clearIntervalAsync(interval)
          } 
        }).catch(error => {
          console.log(error);
        })
      }

      
      interval = setIntervalAsync(fetchData, 3000)
    }, [])
  
    const handleExpandClick = () => {
      setExpanded(!expanded);
    };

    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
      setAnchorEl(null);
    };

    if(loading) return <Loading/>
  
    return (
    <Grid container>
        <Grid item xs={12} style={{ paddingTop: "1rem", display:'flex', justifyContent:'center'}}>
            <Card className={classes.root}>
                <CardHeader
                avatar={
                    <Avatar aria-label="recipe" className={classes.avatar}>
                        <PersonIcon />
                    </Avatar>
                }
                action={
                    <IconButton aria-label="settings">
                        <MoreVertIcon />
                    </IconButton>
                }
                title={`Bud with your friend ${friend.username}`} 
                subheader={ date_today.toLocaleDateString() }
                />
                <CardMedia
                className={classes.media}
                image={imagePath + img + ".png"} 
                title="Result"
                />
                <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">
                  Digite aqui sua memória com seu bud {friend.username}! 
                </Typography>
                </CardContent>
                <CardActions disableSpacing>
                <IconButton aria-label="add to favorites">
                    <FavoriteIcon />
                </IconButton>
                <IconButton aria-label="share" onClick={handleClick}>
                    <ShareIcon />
                </IconButton>
                <StyledMenu
                  id="customized-menu"
                  anchorEl={anchorEl}
                  keepMounted
                  open={Boolean(anchorEl)}
                  onClose={handleClose}
                  >
                  <StyledMenuItem>
                    <ListItemIcon>
                      <FacebookIcon fontSize="small" />
                    </ListItemIcon>
                    <ListItemText primary="Facebook" />
                  </StyledMenuItem>
                  <StyledMenuItem>
                    <ListItemIcon>
                      <WhatsAppIcon fontSize="small" />
                    </ListItemIcon>
                    <ListItemText primary="Whatsapp" />
                  </StyledMenuItem>
                  <StyledMenuItem>
                    <ListItemIcon>
                      <InstagramIcon fontSize="small" />
                    </ListItemIcon>
                    <ListItemText primary="Instagram" />
                  </StyledMenuItem>
                </StyledMenu>
              </CardActions>
            </Card>
        </Grid>
        <Grid item xs={12} style={{ paddingTop: "1rem", display:'flex', justifyContent:'center'}}>
          <Rewards />
        </Grid>
      </Grid>
    );
};