import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import LinearProgress from "@material-ui/core/LinearProgress";
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    display: 'flex',
    '& > * + *': {
      marginLeft: theme.spacing(2),
      
    },
  },
  colorPrimary: {
    backgroundColor: '#FFF',
  },
  bar: {
    borderRadius: 5,
    backgroundColor: '#D39827',
  },

}))(LinearProgress);


const useStyles = makeStyles({
  root: {
    flexGrow: 1
  }
});

export default function CustomizedProgressBars() {
  const classes = useStyles();

  return (
    <div style={{position: 'relative'}}>
      <p>34 brindes! Uau! Você atingiu o nível 4 de Bud!</p>
      <BorderLinearProgress variant="determinate" value={50} />
      <p>Seu próximo prêmio: um cupom de desconto!</p>
	</div>
  );
}
